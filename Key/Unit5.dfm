object FormList2: TFormList2
  Left = 177
  Top = 275
  Width = 332
  Height = 392
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderStyle = bsSizeToolWin
  Caption = 'ASCII ('#1082#1086#1076#1080#1088#1086#1074#1082#1072' DOS)'
  Color = clBtnFace
  Constraints.MinHeight = 237
  Constraints.MinWidth = 332
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object SpeedButton1: TSpeedButton
    Left = 160
    Top = 335
    Width = 76
    Height = 22
    Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100
    OnClick = SpeedButton1Click
  end
  object SpeedButton2: TSpeedButton
    Left = 240
    Top = 335
    Width = 76
    Height = 22
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100
    OnClick = SpeedButton2Click
  end
  object ListView1: TListView
    Left = 5
    Top = 10
    Width = 271
    Height = 191
    Columns = <
      item
        Caption = 'Dec'
      end
      item
        Caption = 'Hex'
      end
      item
        Caption = 'Symbol'
      end>
    ColumnClick = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Terminal'
    Font.Style = []
    FlatScrollBars = True
    GridLines = True
    ReadOnly = True
    RowSelect = True
    ParentFont = False
    PopupMenu = PopupMenu1
    TabOrder = 0
    ViewStyle = vsReport
    OnDblClick = N2Click
  end
  object Edit1: TEdit
    Left = 5
    Top = 335
    Width = 146
    Height = 20
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Terminal'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  object PopupMenu1: TPopupMenu
    Left = 15
    Top = 35
    object N1: TMenuItem
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1089#1080#1084#1074#1086#1083
      OnClick = N1Click
    end
    object N2: TMenuItem
      Caption = #1042#1099#1073#1088#1072#1090#1100' '#1076#1083#1103' '#1082#1086#1087#1080#1088#1086#1074#1072#1085#1080#1103
      OnClick = N2Click
    end
  end
end
