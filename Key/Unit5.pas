unit Unit5;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ComCtrls, Clipbrd, Buttons, StdCtrls;

type
  TFormList2 = class(TForm)
    ListView1: TListView;
    Edit1: TEdit;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormList2: TFormList2;

implementation

uses Unit1;

{$R *.dfm}

procedure TFormList2.FormCreate(Sender: TObject);
var i: Byte;
begin
FormList2.Left:=FormMain.Left+15;
FormList2.Top:=FormMain.Top+25;
for i:=0 to 255 do
  begin
  with ListView1.Items.Add do
    begin
    Caption:=IntToStr(i);
    SubItems.Add(IntToHex(i,2));
    SubItems.Add(Chr(i));
    end;
  end;
end;

procedure TFormList2.FormResize(Sender: TObject);
begin
ListView1.Width:=FormList2.Width-19;
ListView1.Height:=FormList2.Height-75;
ListView1.Columns[2].Width:=FormList2.Width-143;

Edit1.Width:=FormList2.Width-186;
SpeedButton1.Left:=FormList2.Width-172;
SpeedButton2.Left:=FormList2.Width-92;
Edit1.Top:=FormList2.Height-57;
SpeedButton1.Top:=FormList2.Height-57;
SpeedButton2.Top:=FormList2.Height-57;
end;

procedure TFormList2.N1Click(Sender: TObject);
begin
Clipboard.SetTextBuf(PChar(ListView1.Items[ListView1.ItemIndex].SubItems[1]));
end;

procedure TFormList2.N2Click(Sender: TObject);
begin
Edit1.Text:=Edit1.Text+Chr(StrToInt(ListView1.Items[ListView1.ItemIndex].Caption));
end;

procedure TFormList2.SpeedButton1Click(Sender: TObject);
begin
Clipboard.SetTextBuf(PChar(Edit1.Text));
end;

procedure TFormList2.SpeedButton2Click(Sender: TObject);
begin
Edit1.Text:='';
end;

end.
