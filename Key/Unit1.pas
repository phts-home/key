unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ShellAPI, Menus, IniFiles;

type
  TFormMain = class(TForm)
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Edit4: TEdit;
    Label4: TLabel;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    KeyDosVersion1: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    ASCII1: TMenuItem;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure KeyDosVersion1Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    ProgCfg: TIniFile;
    { Public declarations }
  end;

var
  FormMain: TFormMain;

implementation

uses Unit2, Unit3, Unit4, Unit5;

{$R *.dfm}

procedure TFormMain.FormKeyPress(Sender: TObject; var Key: Char);
begin
if (key <> #13)and(key <> #27)and(key <> #8)and(key <> #32)
  then edit1.text:=key;
edit3.Text:=inttostr(ord(key));
edit4.Text:=inttohex(ord(key),2);
end;

procedure TFormMain.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if key=9
  then
  begin
  edit1.text:='Tab';
  edit2.text:=inttostr(key);
  edit3.Text:='9';
  edit4.Text:='9';
  end;
end;

procedure TFormMain.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
edit2.text:=inttostr(key);
case key of
  112:begin
      edit1.text:='F1';
      edit3.Text:='';
      edit4.Text:='';
      end;
  113:begin
      edit1.text:='F2';
      edit3.Text:='';
      edit4.Text:='';
      end;
  114:begin
      edit1.text:='F3';
      edit3.Text:='';
      edit4.Text:='';
      end;
  115:begin
      edit1.text:='F4';
      edit3.Text:='';
      edit4.Text:='';
      end;
  116:begin
      edit1.text:='F5';
      edit3.Text:='';
      edit4.Text:='';
      end;
  117:begin
      edit1.text:='F6';
      edit3.Text:='';
      edit4.Text:='';
      end;
  118:begin
      edit1.text:='F7';
      edit3.Text:='';
      edit4.Text:='';
      end;
  119:begin
      edit1.text:='F8';
      edit3.Text:='';
      edit4.Text:='';
      end;
  120:begin
      edit1.text:='F9';
      edit3.Text:='';
      edit4.Text:='';
      end;
  121:begin
      edit1.text:='F10';
      edit3.Text:='';
      edit4.Text:='';
      end;
  122:begin
      edit1.text:='F11';
      edit3.Text:='';
      edit4.Text:='';
      end;
  123:begin
      edit1.text:='F12';
      edit3.Text:='';
      edit4.Text:='';
      end;
  145:begin
      edit1.text:='Scroll Lock';
      edit3.Text:='';
      edit4.Text:='';
      end;
  19: begin
      edit1.text:='Pause';
      edit3.Text:='';
      edit4.Text:='';
      end;
  20: begin
      edit1.text:='Caps Lock';
      edit3.Text:='';
      edit4.Text:='';
      end;
  16: begin
      edit1.text:='Shift';
      edit3.Text:='';
      edit4.Text:='';
      end;
  17: begin
      edit1.text:='Ctrl';
      edit3.Text:='';
      edit4.Text:='';
      end;
  91: begin
      edit1.text:='Windows';
      edit3.Text:='';
      edit4.Text:='';
      end;
  92: begin
      edit1.text:='Windows';
      edit3.Text:='';
      edit4.Text:='';
      end;
  93: begin
      edit1.text:='Context';
      edit3.Text:='';
      edit4.Text:='';
      end;
  18: begin
      edit1.text:='Alt';
      edit3.Text:='';
      edit4.Text:='';
      end;
  45: begin
      edit1.text:='Insert';
      edit3.Text:='';
      edit4.Text:='';
      end;
  46: begin
      edit1.text:='Delete';
      edit3.Text:='';
      edit4.Text:='';
      end;
  36: begin
      edit1.text:='Home';
      edit3.Text:='';
      edit4.Text:='';
      end;
  35: begin
      edit1.text:='End';
      edit3.Text:='';
      edit4.Text:='';
      end;
  33: begin
      edit1.text:='Page Up';
      edit3.Text:='';
      edit4.Text:='';
      end;
  34: begin
      edit1.text:='Page Down';
      edit3.Text:='';
      edit4.Text:='';
      end;
  38: begin
      edit1.text:='Up';
      edit3.Text:='';
      edit4.Text:='';
      end;
  37: begin
      edit1.text:='Left';
      edit3.Text:='';
      edit4.Text:='';
      end;
  40: begin
      edit1.text:='Down';
      edit3.Text:='';
      edit4.Text:='';
      end;
  39: begin
      edit1.text:='Right';
      edit3.Text:='';
      edit4.Text:='';
      end;
  144:begin
      edit1.text:='Num Lock';
      edit3.Text:='';
      edit4.Text:='';
      end;


  13: edit1.text:='Enter';
  27: edit1.text:='Esc';
  32: edit1.text:='Space';
  8:  edit1.text:='Backspace';
  44: begin
      edit1.text:='Print Screen';
      edit3.Text:='';
      edit4.Text:='';
      end
  else
    begin
    edit1.Text:='';
    edit3.Text:='';
    edit4.Text:='';
    end;
  end

end;

procedure TFormMain.SpeedButton3Click(Sender: TObject);
begin
if FormList.Showing then FormList.Close else FormList.Show;
end;

procedure TFormMain.SpeedButton4Click(Sender: TObject);
begin
if FormList2.Showing then FormList2.Close else FormList2.Show;
end;

procedure TFormMain.N7Click(Sender: TObject);
begin
FormText.ShowModal;
end;

procedure TFormMain.KeyDosVersion1Click(Sender: TObject);
begin
if FileExists(ExtractFilePath(ParamStr(0))+'Key_DosVersion.exe')
  then ShellExecute(0,'',PChar(ExtractFilePath(ParamStr(0))+'Key_DosVersion.exe'),'','',1)
  else Application.MessageBox(PChar('������ ��� ������ ����� ��� ���� �� ������'+#13+ExtractFilePath(ParamStr(0))+'Key_DosVersion.exe'),PChar(FormMain.Caption),MB_ICONERROR+mb_OK);
end;

procedure TFormMain.N4Click(Sender: TObject);
begin
Close;
end;

procedure TFormMain.N3Click(Sender: TObject);
begin
FormAbout.ShowModal;
end;

procedure TFormMain.FormCreate(Sender: TObject);
begin
ProgCfg:=TIniFile.Create(ExtractFilePath(ParamStr(0))+'Key.ini');
Left:=ProgCfg.ReadInteger('form','left',325);
Top:=ProgCfg.ReadInteger('form','top',290);
end;

procedure TFormMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
ProgCfg.WriteInteger('form','left',Left);
ProgCfg.WriteInteger('form','top',Top);
end;

end.
