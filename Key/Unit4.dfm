object FormText: TFormText
  Left = 346
  Top = 301
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #1058#1077#1082#1089#1090
  ClientHeight = 154
  ClientWidth = 267
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Edit1: TEdit
    Left = 10
    Top = 10
    Width = 246
    Height = 21
    BiDiMode = bdLeftToRight
    ParentBiDiMode = False
    TabOrder = 0
    OnChange = Edit1Change
  end
  object Memo1: TMemo
    Left = 10
    Top = 35
    Width = 246
    Height = 106
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 1
  end
end
