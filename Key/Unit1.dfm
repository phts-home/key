object FormMain: TFormMain
  Left = 325
  Top = 290
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Key'
  ClientHeight = 118
  ClientWidth = 267
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  Menu = MainMenu1
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnKeyUp = FormKeyUp
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 10
    Top = 15
    Width = 39
    Height = 13
    Caption = #1057#1080#1084#1074#1086#1083
  end
  object Label2: TLabel
    Left = 10
    Top = 40
    Width = 58
    Height = 13
    Caption = #8470' '#1082#1083#1072#1074#1080#1096#1080
  end
  object Label3: TLabel
    Left = 10
    Top = 65
    Width = 52
    Height = 13
    Caption = 'ANSI (dec)'
  end
  object Label4: TLabel
    Left = 10
    Top = 90
    Width = 51
    Height = 13
    Caption = 'ANSI (hex)'
  end
  object SpeedButton3: TSpeedButton
    Left = 180
    Top = 10
    Width = 76
    Height = 22
    Caption = 'ANSI'
    OnClick = SpeedButton3Click
  end
  object SpeedButton4: TSpeedButton
    Left = 180
    Top = 35
    Width = 76
    Height = 22
    Caption = 'ASCII'
    OnClick = SpeedButton4Click
  end
  object Edit1: TEdit
    Left = 80
    Top = 10
    Width = 81
    Height = 21
    TabStop = False
    BiDiMode = bdLeftToRight
    Enabled = False
    ParentBiDiMode = False
    ReadOnly = True
    TabOrder = 0
  end
  object Edit2: TEdit
    Left = 80
    Top = 35
    Width = 81
    Height = 21
    TabStop = False
    Enabled = False
    ReadOnly = True
    TabOrder = 1
  end
  object Edit3: TEdit
    Left = 80
    Top = 60
    Width = 81
    Height = 21
    TabStop = False
    Enabled = False
    ReadOnly = True
    TabOrder = 2
  end
  object Edit4: TEdit
    Left = 80
    Top = 85
    Width = 81
    Height = 21
    TabStop = False
    Enabled = False
    ReadOnly = True
    TabOrder = 3
  end
  object MainMenu1: TMainMenu
    Left = 180
    Top = 60
    object N1: TMenuItem
      Caption = #1055#1088#1086#1075#1088#1072#1084#1084#1072
      object N6: TMenuItem
        Caption = #1057#1087#1080#1089#1086#1082' ANSI'
        OnClick = SpeedButton3Click
      end
      object ASCII1: TMenuItem
        Caption = #1057#1087#1080#1089#1086#1082' ASCII'
        OnClick = SpeedButton4Click
      end
      object N7: TMenuItem
        Caption = #1058#1077#1082#1089#1090'...'
        OnClick = N7Click
      end
      object N8: TMenuItem
        Caption = '-'
      end
      object KeyDosVersion1: TMenuItem
        Caption = #1047#1072#1087#1091#1089#1090#1080#1090#1100' Key DosVersion'
        OnClick = KeyDosVersion1Click
      end
      object N5: TMenuItem
        Caption = '-'
      end
      object N4: TMenuItem
        Caption = #1042#1099#1093#1086#1076
        OnClick = N4Click
      end
    end
    object N2: TMenuItem
      Caption = #1057#1087#1088#1072#1074#1082#1072
      object N3: TMenuItem
        Caption = #1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077'...'
        OnClick = N3Click
      end
    end
  end
end
