program Key;

uses
  Forms,
  Unit1 in 'Unit1.pas' {FormMain},
  Unit2 in 'Unit2.pas' {FormAbout},
  Unit3 in 'Unit3.pas' {FormList},
  Unit4 in 'Unit4.pas' {FormText},
  Unit5 in 'Unit5.pas' {FormList2};
//  Unit6 in 'Unit6.pas' {FormUserList};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'Key';
  Application.CreateForm(TFormMain, FormMain);
  Application.CreateForm(TFormAbout, FormAbout);
  Application.CreateForm(TFormList, FormList);
  Application.CreateForm(TFormText, FormText);
  Application.CreateForm(TFormList2, FormList2);
  Application.Run;
end.
