unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ShellApi;

type
  TFormAbout = class(TForm)
    Image1: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label7: TLabel;
    Label10: TLabel;
    procedure FormShow(Sender: TObject);
    procedure Button_closeClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Label4Click(Sender: TObject);
    procedure Label7Click(Sender: TObject);
    procedure FormClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormAbout: TFormAbout;

implementation

uses Unit1;

{$R *.dfm}

procedure TFormAbout.FormShow(Sender: TObject);
begin
FormAbout.Left:=FormMain.left+15;
FormAbout.Top:=FormMain.top+25;
end;

procedure TFormAbout.Button_closeClick(Sender: TObject);
begin
close;
end;

procedure TFormAbout.Timer1Timer(Sender: TObject);
begin
label1.Font.Color:=random($ffffff);
end;

procedure TFormAbout.Label4Click(Sender: TObject);
begin
ShellExecute(0,pchar(''),pchar('mailto:'+Label4.Caption+'?subject=Key 2.1'),pchar(''),pchar(''),1);
end;

procedure TFormAbout.Label7Click(Sender: TObject);
begin
ShellExecute(0,pchar(''),pchar(Label7.Caption),pchar(''),pchar(''),1);
end;

procedure TFormAbout.FormClick(Sender: TObject);
begin
Close;
end;

end.
