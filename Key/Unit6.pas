unit Unit6;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ComCtrls, Buttons, StdCtrls;

type
  TFormUserList = class(TForm)
    Edit1: TEdit;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    ListView1: TListView;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    SpeedButton3: TSpeedButton;
    FontDialog1: TFontDialog;
    procedure SpeedButton3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    procedure WriteCodes;
    { Public declarations }
  end;

var
  FormUserList: TFormUserList;

implementation

{$R *.dfm}

procedure TFormUserList.SpeedButton3Click(Sender: TObject);
begin
if FontDialog1.Execute then
  begin
  ListView1.Font:=FontDialog1.Font;
//  WriteCodes;
  end;

end;

procedure TFormUserList.FormCreate(Sender: TObject);
begin
WriteCodes;
end;

procedure TFormUserList.WriteCodes;
var i: Byte;
begin
ListView1.Clear;
for i:=0 to 255 do
  begin
  with ListView1.Items.Add do
    begin
    Caption:=IntToStr(i);
    SubItems.Add(IntToHex(i,2));
    SubItems.Add(Chr(i));
    end;
  end;
end;

end.
