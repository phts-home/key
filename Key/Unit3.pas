unit Unit3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, Menus, Clipbrd, Buttons;

type
  TFormList = class(TForm)
    ListView1: TListView;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Edit1: TEdit;
    N2: TMenuItem;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormList: TFormList;

implementation

uses Unit1;

{$R *.dfm}

procedure TFormList.FormCreate(Sender: TObject);
var i: Byte;
    s: String;
begin
FormList.Left:=FormMain.Left+15;
FormList.Top:=FormMain.Top+25;
for i:=0 to 31 do
  begin
  case i of
  0: s:= 'NUL (null)';
  1: s:= 'SOH (start of heading)';
  2: s:= 'STX (start of text)';
  3: s:= 'ETX (end of text)';
  4: s:= 'EOT (end of transmission)';
  5: s:= 'ENQ (enquiry)';
  6: s:= 'ACK (acknowledge)';
  7: s:= 'BEL (bell)';
  8: s:= ' BS (backspace)';
  9: s:= 'TAB (horizontal tab)';
  10: s:= ' LF (NL line feed, new line)';
  11: s:= ' VT (vertical tab)';
  12: s:= ' FF (NP form feed, new page)';
  13: s:= ' CR (carriage return)';
  14: s:= ' SO (shift out)';
  15: s:= ' SI (shift in)';
  16: s:= 'DLE (data link escape)';
  17: s:= 'DC1 (device control 1)';
  18: s:= 'DC2 (device control 2)';
  19: s:= 'DC3 (device control 3)';
  20: s:= 'DC4 (device control 4)';
  21: s:= 'NAK (negative acknowledge)';
  22: s:= 'SYN (synchronous idle)';
  23: s:= 'ETB (end of trans. block)';
  24: s:= 'CAN (cancel)';
  25: s:= ' EM (end of medium)';
  26: s:= 'SUB (substitute)';
  27: s:= 'ESC (escape)';
  28: s:= ' FS (file separator)';
  29: s:= ' GS (group separator)';
  30: s:= ' RS (record separator)';
  31: s:= ' US (unit separator)';
    end;
  with ListView1.Items.Add do
    begin
    Caption:=inttostr(i);
    SubItems.Add(IntToHex(i,2));
    SubItems.Add(s);
    end;
  end;
for i:=32 to 255 do
  begin
  with ListView1.Items.Add do
    begin
    Caption:=IntToStr(i);
    SubItems.Add(IntToHex(i,2));
    SubItems.Add(Chr(i));
    end;
  end;
end;

procedure TFormList.FormResize(Sender: TObject);
begin
ListView1.Width:=FormList.Width-19;
ListView1.Height:=FormList.Height-75;
ListView1.Columns[2].Width:=FormList.Width-133;
Edit1.Width:=FormList.Width-186;
SpeedButton1.Left:=FormList.Width-172;
SpeedButton2.Left:=FormList.Width-92;
Edit1.Top:=FormList.Height-57;
SpeedButton1.Top:=FormList.Height-57;
SpeedButton2.Top:=FormList.Height-57;
end;

procedure TFormList.N1Click(Sender: TObject);
begin
Clipboard.SetTextBuf(PChar(ListView1.Items[ListView1.ItemIndex].SubItems[1]));
end;

procedure TFormList.N2Click(Sender: TObject);
begin
Edit1.Text:=Edit1.Text+Chr(StrToInt(ListView1.Items[ListView1.ItemIndex].Caption));
end;

procedure TFormList.SpeedButton1Click(Sender: TObject);
begin
Clipboard.SetTextBuf(PChar(Edit1.Text));
end;

procedure TFormList.SpeedButton2Click(Sender: TObject);
begin
Edit1.Text:='';
end;

end.
