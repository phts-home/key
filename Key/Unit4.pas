unit Unit4;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, StdCtrls;

type
  TFormText = class(TForm)
    Edit1: TEdit;
    Memo1: TMemo;
    procedure Edit1Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormText: TFormText;

implementation

uses Unit1;

{$R *.dfm}

procedure TFormText.Edit1Change(Sender: TObject);
var i: Integer;
begin
Memo1.Clear;
for i:=1 to Length(Edit1.Text) do
  begin
  Memo1.Text:=Memo1.Text+'#'+IntToStr(Ord(Edit1.Text[i]));
  end;
end;

procedure TFormText.FormShow(Sender: TObject);
begin
FormText.Left:=FormMain.left+15;
FormText.Top:=FormMain.top+25;
Edit1.SetFocus;
end;

end.
