object FormUserList: TFormUserList
  Left = 355
  Top = 212
  Width = 332
  Height = 392
  BorderStyle = bsSizeToolWin
  Caption = #1050#1086#1076#1080#1088#1086#1074#1082#1072' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1103
  Color = clBtnFace
  Constraints.MinHeight = 237
  Constraints.MinWidth = 332
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object SpeedButton1: TSpeedButton
    Left = 160
    Top = 335
    Width = 76
    Height = 22
    Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100
  end
  object SpeedButton2: TSpeedButton
    Left = 240
    Top = 335
    Width = 76
    Height = 22
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100
  end
  object SpeedButton3: TSpeedButton
    Left = 160
    Top = 305
    Width = 76
    Height = 22
    Caption = #1064#1088#1080#1092#1090
    OnClick = SpeedButton3Click
  end
  object Edit1: TEdit
    Left = 5
    Top = 335
    Width = 146
    Height = 21
    TabOrder = 0
  end
  object ListView1: TListView
    Left = 5
    Top = 10
    Width = 236
    Height = 211
    Columns = <
      item
        Caption = 'Dec'
        Width = 45
      end
      item
        Caption = 'Hex'
        Width = 45
      end
      item
        Caption = #1057#1080#1084#1074#1086#1083
      end>
    ColumnClick = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Courier New'
    Font.Style = []
    FlatScrollBars = True
    GridLines = True
    ReadOnly = True
    RowSelect = True
    ParentFont = False
    PopupMenu = PopupMenu1
    TabOrder = 1
    ViewStyle = vsReport
  end
  object PopupMenu1: TPopupMenu
    Left = 15
    Top = 35
    object N1: TMenuItem
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1089#1080#1084#1074#1086#1083
    end
    object N2: TMenuItem
      Caption = #1042#1099#1073#1088#1072#1090#1100' '#1076#1083#1103' '#1082#1086#1087#1080#1088#1086#1074#1072#1085#1080#1103
    end
  end
  object FontDialog1: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Left = 45
    Top = 35
  end
end
