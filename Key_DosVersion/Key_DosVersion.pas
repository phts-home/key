program Key_DosVersion;
uses Crt, Graph;
var k: Char;
    CItem: Byte;

function DecToHex (Value: Integer): String;
const HS: array[0..15] of Char = '0123456789ABCDEF';
begin
DecToHex := '';
while Value > 0 do
  begin
  DecToHex := HS[Value mod 16] + DecToHex;
  Value := Value shr 4;
  end;
if DecToHex = '' then DecToHex:='00';
if Length(DecToHex) = 1 then DecToHex:='0' + DecToHex;
end;

procedure MainMenu (Item: Byte);
var i: Byte;
begin
ClrScr;
Write(#201);
for i:=0 to 33 do
  begin
  Write(#205);
  end;
WriteLn(#187);
WriteLn(#186#32#177#177#177#177#177#177#177+'  Key DosVersion  '+#177#177#177#177#177#177#177#32#186);
Write(#199);
for i:=0 to 33 do
  begin
  Write(#196);
  end;
WriteLn(#182);
Write(#186);
for i:=0 to 33 do
  begin
  Write(#32);
  end;
WriteLn(#186);
case Item of
  0:begin
    WriteLn(#186+'  >  Read Key Codes               '+#186);
    WriteLn(#186+'     List                         '+#186);
    WriteLn(#186+'     About                        '+#186);
    WriteLn(#186+'     Exit                         '+#186);
    end;
  1:begin
    WriteLn(#186+'     Read Key Codes               '+#186);
    WriteLn(#186+'  >  List                         '+#186);
    WriteLn(#186+'     About                        '+#186);
    WriteLn(#186+'     Exit                         '+#186);
    end;
  2:begin
    WriteLn(#186+'     Read Key Codes               '+#186);
    WriteLn(#186+'     List                         '+#186);
    WriteLn(#186+'  >  About                        '+#186);
    WriteLn(#186+'     Exit                         '+#186);
    end;
  3:begin
    WriteLn(#186+'     Read Key Codes               '+#186);
    WriteLn(#186+'     List                         '+#186);
    WriteLn(#186+'     About                        '+#186);
    WriteLn(#186+'  >  Exit                         '+#186);
    end;
  end;
Write(#186);
for i:=0 to 33 do
  begin
  Write(#32);
  end;
WriteLn(#186);
Write(#200);
for i:=0 to 33 do
  begin
  Write(#205);
  end;
WriteLn(#188);
WriteLn;
WriteLn(#179#32#24+'/'+#25+'   :  Cursor up/down');
WriteLn(#179+' Enter :  Select item');
WriteLn(#179+' 1..4  :  Select item');
WriteLn(#179+' Esc   :  Exit');
end;

procedure ReadKeyCode;
var c, Ans: Char;
    s: String;
    i: Byte;
    EscPressed: Boolean;
begin
ClrScr;
Write(#201);
for i:=0 to 33 do
  begin
  Write(#205);
  end;
WriteLn(#187);
WriteLn(#186#32#177#177#177#177#177#177#177+'  Key DosVersion  '+#177#177#177#177#177#177#177#32#186);
Write(#211);
for i:=0 to 33 do
  begin
  Write(#196);
  end;
WriteLn(#189);
WriteLn;
WriteLn(#179+' Esc : Exit to menu');
WriteLn;

Write(#218);
for i:=0 to 33 do
  begin
  Write(#196);
  end;
WriteLn(#191);

WriteLn(#179+'          Symbol | Hex | Dec      '+#179);
WriteLn;

while True do
  begin
  EscPressed:=False;
  Write('  > ');
  c:=ReadKey;
  case c of
    #8:  s:='Backspace';
    #9:  s:='Tab';
    #10: s:='LF  (NL line feed, new line)';
    #13: s:='Enter';
    #27: begin s:='Esc'; EscPressed:=True end;
    else s:=c;
    end;
  DelLine;
  if c=#0 then
    begin
    c:=ReadKey;
    case c of
      #2..#10:s:='Ctrl + '+Chr(Ord(c)+47);
      #11: s:='Ctrl + 0';
      #12: s:='Ctrl + -';
      #13: s:='Ctrl + =';
      #16: s:='Alt + q';
      #17: s:='Alt + w';
      #18: s:='Alt + e';
      #19: s:='Alt + r';
      #20: s:='Alt + t';
      #21: s:='Alt + y';
      #22: s:='Alt + u';
      #23: s:='Alt + i';
      #24: s:='Alt + o';
      #25: s:='Alt + p';
      #26: s:='Alt + [';
      #27: s:='Alt + ]';

      #30: s:='Alt + a';
      #31: s:='Alt + s';
      #32: s:='Alt + d';
      #33: s:='Alt + f';
      #34: s:='Alt + g';
      #35: s:='Alt + h';
      #36: s:='Alt + j';
      #37: s:='Alt + k';
      #38: s:='Alt + l';
      #39: s:='Alt + ;';
      #40: s:='Alt + '+#39;

      #41: s:='Ctrl + `';

      #43: s:='Alt + \';

      #44: s:='Alt + z';
      #45: s:='Alt + x';
      #46: s:='Alt + c';
      #47: s:='Alt + v';
      #48: s:='Alt + b';
      #49: s:='Alt + n';
      #50: s:='Alt + m';

      #51: s:='Ctrl + ,';
      #52: s:='Ctrl + .';
      #53: s:='Numpad /';
      #59: s:='F1';
      #60: s:='F2';
      #61: s:='F3';
      #62: s:='F4';
      #63: s:='F5';
      #64: s:='F6';
      #65: s:='F7';
      #66: s:='F8';
      #67: s:='F9';
      #68: s:='F10';
      #71: s:='Home';
      #72: s:='Up';
      #73: s:='Page Up';
      #75: s:='Left';
      #76: s:='Fire';
      #77: s:='Right';
      #79: s:='End';
      #80: s:='Down';
      #81: s:='Page Down';
      #82: s:='Insert';
      #83: s:='Delete';
      #91: s:='Left Windows';
      #92: s:='Right Windows';
      #93: s:='Context';
      #94: s:='Ctrl + F1';
      #95: s:='Ctrl + F2';
      #96: s:='Ctrl + F3';
      #97: s:='Ctrl + F4';
      #98: s:='Ctrl + F5';
      #99: s:='Ctrl + F6';
      #100: s:='Ctrl + F7';
      #101: s:='Ctrl + F8';
      #102: s:='Ctrl + F9';
      #103: s:='Ctrl + F10';
      #104: s:='Alt + F1';
      #105: s:='Alt + F2';
      #106: s:='Alt + F3';
      #107: s:='Alt + F4';
      #108: s:='Alt + F5';
      #109: s:='Alt + F6';
      #110: s:='Alt + F7';
      #111: s:='Alt + F8';
      #112: s:='Alt + F9';
      #113: s:='Alt + F10';
      #115:s:='Ctrl + Right';
      #116:s:='Ctrl + Left';
      #117:s:='Ctrl + End';
      #118:s:='Ctrl + Page Down';
      #119:s:='Ctrl + Home';
      #120..#128: s:='Alt + '+Chr(Ord(c)-71);
      #129: s:='Alt + 0';
      #132:s:='Ctrl + Page Up';
      #133: s:='F11';
      #134: s:='F12';
      #137: s:='Ctrl + F11';
      #138: s:='Ctrl + F12`';
      #141: s:='Ctrl + Up';
      #145: s:='Ctrl + Down';
      #146: s:='Ctrl + Insert';
      #147: s:='Ctrl + Delete';
      #148: s:='Alt + F11';
      #149: s:='Alt + F12';
      #164: s:='Alt + /';
      #167: s:='Alt + /';
      end;
    WriteLn(s:13,{' | ',DecToHex(Ord(c)),}' |     | #0 + #',Ord(c));
    end else WriteLn(s:13,' |  ',DecToHex(Ord(c)),' | #',Ord(c));
  if EscPressed then
    begin
    Write('  > Exit? [y,n] ');
    repeat
      Ans:=ReadKey;
      until (UpCase(Ans)='Y')or(UpCase(Ans)='N')or(Ans=#27)or(Ans=#13);
    if (UpCase(Ans)='Y')or(Ans=#13) then Exit else begin DelLine;GotoXY(1,WhereY);end;
    end;
  end;
end;

procedure WriteList;
var i: Byte;
    m: Char;
    s: String;
begin
ClrScr;
Write(#201);
for i:=0 to 33 do
  begin
  Write(#205);
  end;
WriteLn(#187);
WriteLn(#186#32#177#177#177#177#177#177#177+'  Key DosVersion  '+#177#177#177#177#177#177#177#32#186);
Write(#211);
for i:=0 to 33 do
  begin
  Write(#196);
  end;
WriteLn(#189);
WriteLn;
WriteLn(#179+' Any key : Next symbol');
WriteLn(#179+' Tab     : Full list');
WriteLn(#179+' Esc     : Exit to menu');
WriteLn;

Write(#218);
for i:=0 to 33 do
  begin
  Write(#196);
  end;
WriteLn(#191);

WriteLn(#179+'        Dec | Hex | Symbol        '+#179);

WriteLn;

for i:=0 to 255 do
  begin
  if m<>#9 then
    begin
    m:=ReadKey;
    if m=#27 then Exit;
    end;
  case i of
    0:  s:='NUL (null)';
    8:  s:='BS  (backspace)';
    9:  s:='TAB (horisontal tab)';
    10: s:='LF  (NL line feed, new line)';
    13: s:='CR  (carriage return)';
    27: s:=Chr(i)+' = Esc';
    else s:=Chr(i);
    end;
  WriteLn('        #',i:3,' |  ',DecToHex(i),' | ',s);
  end;
WriteLn;
Write('  > Press any key to exit to menu ');
ReadKey;
end;

procedure ShowAbout;
var i: Byte;
begin
ClrScr;

Write(#201);
for i:=0 to 33 do
  begin
  Write(#205);
  end;
WriteLn(#187);
WriteLn(#186#32#177#177#177#177#177#177#177+'  Key DosVersion  '+#177#177#177#177#177#177#177#32#186);
Write(#199);
for i:=0 to 33 do
  begin
  Write(#196);
  end;
WriteLn(#182);
Write(#186);
for i:=0 to 33 do
  begin
  Write(#32);
  end;
WriteLn(#186);

WriteLn(#186+' Version: 1.0 (Build 2)           '+#186);
Write(#186);
for i:=0 to 33 do
  begin
  Write(#32);
  end;
WriteLn(#186);
WriteLn(#186+' Author: Phil Tsarik              '+#186);
WriteLn(#186+' E-mail: philip-s@yandex.ru       '+#186);
WriteLn(#186+' Site:   http://philip-s.narod.ru '+#186);
Write(#186);
for i:=0 to 33 do
  begin
  Write(#32);
  end;
WriteLn(#186);
WriteLn(#186+' Freeware                         '+#186);

Write(#186);
for i:=0 to 33 do
  begin
  Write(#32);
  end;
WriteLn(#186);
Write(#200);
for i:=0 to 33 do
  begin
  Write(#205);
  end;
WriteLn(#188);

WriteLn;
WriteLn(#179+' Any key : Exit to menu');

ReadKey;
end;

begin
ClrScr;
MainMenu(0);
CItem:=0;
while True do
  begin
  k:=ReadKey;
  case k of
    #0:
      begin
      k:=ReadKey;
      case k of
        #72:begin
            if Citem<>0 then Dec(CItem) else CItem:=3;
              MainMenu(CItem);
            end;
        #80:begin
            if Citem<>3 then Inc(CItem) else Citem:=0;
            MainMenu(CItem);
            end;
        end;
      end;
    #13:
      begin
      case CItem of
        0: ReadKeyCode;
        1: WriteList;
        2: ShowAbout;
        3: Exit;
        end;
      MainMenu(CItem);
      end;
    #27: Exit;
    #49: begin CItem:=0; ReadKeyCode; MainMenu(CItem);end;
    #50: begin CItem:=1; WriteList; MainMenu(CItem);end;
    #51: begin CItem:=2; ShowAbout; MainMenu(CItem);end;
    #52: begin CItem:=3; Exit; MainMenu(CItem);end;
    end;
  end;
end.
